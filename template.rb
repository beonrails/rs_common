gem 'rs_common', :git => 'git@rocketscience.it:rs_common.git'
gem 'therubyracer'

run 'bundle install'

rake "db:create",  :env => 'development'
rake "db:create",  :env => 'test'

run 'rails g rs_common:install'
run 'rails g rs_toolbar:install'
run 'rails g rs_pages:install'

run 'rake db:migrate db:seed'