require 'rails/generators'
require 'rails/generators/rails/app/app_generator'

module RsCommon
  class DummyGenerator < ::Rails::Generators::Base
    desc "Creates blank Rails application, installs RsCommon, and all sample data"

    def self.source_paths
      paths = self.superclass.source_paths
      paths << File.expand_path('../templates', __FILE__)
      paths.flatten
    end

    PASSTHROUGH_OPTIONS = [
      :skip_active_record, :skip_javascript, :database,
      :javascript, :quiet, :pretend, :force, :skip
    ]

    def generate_test_dummy
      opts = (options || {}).slice(*PASSTHROUGH_OPTIONS)
      opts[:force]          = true
      opts[:skip_bundle]    = true
      opts[:old_style_hash] = true
      opts[:skip_test_unit] = true
      opts[:database]       = 'mysql'

      invoke ::Rails::Generators::AppGenerator, [
        File.expand_path(dummy_path, destination_root)
      ], opts
    end

    def test_dummy_install_rs_common
      inside dummy_path do
        run 'bundle install'

        rake "db:create",  :env => 'development'
        rake "db:create",  :env => 'test'

        run 'rails g rs_common:install'
        run 'rails g rs_toolbar:install'
        run 'rails g rs_pages:install'
      end

      directory "app", "#{dummy_path}/app", :force => true
      template "db/migrate/1_create_posts.rb", "#{dummy_path}/db/migrate/1_create_posts.rb", :force => true
      template "config/boot.rb", "#{dummy_path}/config/boot.rb", :force => true
      template "config/application.rb", "#{dummy_path}/config/application.rb", :force => true
      template "Rakefile", "#{dummy_path}/Rakefile", :force => true

      inject_into_file "#{dummy_path}/config/routes.rb", :after => "#{module_name}::Application.routes.draw do\n" do
        <<-CONTENT.gsub(/^ {8}/, '')
          resources :posts
        CONTENT
      end
    end

    def test_dummy_clean
      inside dummy_path do
        remove_file ".gitignore"
        remove_file "doc"
        remove_file "Gemfile"
        remove_file "Gemfile.lock"
        remove_file "lib/tasks"
        remove_file "spec"
        remove_file "public/index.html"
        remove_file "public/robots.txt"
        remove_file "README.rdoc"
        remove_file "vendor"
        remove_file "Capfile"
        remove_file ".rspec"
        remove_file "config/deploy.rb"
        remove_file "config/unicorn.rb"
      end
    end

    protected

      def dummy_path
        'spec/dummy'
      end

      def module_name
        'Dummy'
      end

      def application_definition
        @application_definition ||= begin
          dummy_application_path = File.expand_path("#{dummy_path}/config/application.rb", destination_root)
          unless options[:pretend] || !File.exists?(dummy_application_path)
            contents = File.read(dummy_application_path)
            contents[(contents.index("module #{module_name}"))..-1]
          end
        end
      end
      alias :store_application_definition! :application_definition

      def gemfile_path
        '../../../../Gemfile'
      end
  end
end
