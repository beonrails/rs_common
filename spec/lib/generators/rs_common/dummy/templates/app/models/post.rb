# encoding: utf-8

class Post < ActiveRecord::Base
  has_many :tags
  
  attr_accessible :title
  validates :title, :presence => true

  def to_s
    title
  end
end
