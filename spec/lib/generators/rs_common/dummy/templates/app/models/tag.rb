# encoding: utf-8

class Tag < ActiveRecord::Base
  belongs_to :post

  attr_accessible :name
  validates :name, :presence => true

  def to_s
    title
  end
end
