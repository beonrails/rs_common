# encoding: utf-8

class PostsController < ApplicationController
  # You should add, for example, :class => "YourEngine::ModelClass" if your model has namespace
  # You should add :find_by => :slug if your record searched by slug column, not an id
  load_and_authorize_resource :post
  skip_authorize_resource     :post, :only => [:index, :show]

  respond_to :html

  # Add toolbar actions menu with RsToolbar gem
  rs_toolbar

  def index
    @posts = Post.all
  end

  def show
    # do nothing
  end

  def new
    @post = Post.new
    6.times { @post.tags.build(:name => 'Linux and Unix') }
  end

  def edit
    # do nothing
  end

  def create
    @post = Post.new(params[:post])
    respond_with(@post)
  end

  def update
    @post.update_attributes(params[:post])
    respond_with(@post)
  end

  def destroy
    @post.destroy
    respond_with(@post)
  end

end
