desc "Run tasks in staging enviroment."
task :staging do
  # Staging nodes
  role :web, domain
  role :app, domain
  role :db,  domain, :primary => true

  set :rails_env, "staging"
end