require 'thinking_sphinx/deploy/capistrano'

after  "deploy:restart",            "thinking_sphinx:shared_sphinx_folder"
after  "thinking_sphinx:configure", "thinking_sphinx:index"