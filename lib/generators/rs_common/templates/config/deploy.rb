# Pretty capistrano config.
# Deployment process:
# cap deploy:setup (on first deploy)
# cap deploy
# cap db:seed (on first deploy)

require 'bundler/capistrano'
load 'deploy/assets'

# Common settings
set :application,   '<projectname>'
set :domain,        '<projectname>@rocketscience.it'
set :repository,    'git@rocketscience.it:<projectname>.git'
set :scm,           :git
set :deploy_via,    :remote_cache
set :branch,        'master'
set :scm_username,  '<scm_username>' # e.g. tanraya or oleg
set :scm_verbose,   true
set :user,          '<projectname>'
set :use_sudo,      false
set :keep_releases, 2
set :deploy_to,     "/home/#{user}"

# SSH settings
ssh_options[:forward_agent] = true
default_run_options[:pty]   = false

# Require custom recipes from rs_common gem
require 'rs_common'
load 'deploy/capistrano_colors.rb'
load 'deploy/production.rb'
load 'deploy/staging.rb'
#load 'deploy/whenever.rb'
#load 'deploy/passenger.rb'
load 'deploy/unicorn.rb'
load 'deploy/database.rb'
#load '/deploy/sphinx.rb'

after "bundle:install", "deploy:migrate"
after "bundle:install", "deploy:migrate"
after "deploy",         "deploy:cleanup"
