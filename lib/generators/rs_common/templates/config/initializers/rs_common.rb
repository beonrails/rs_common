# encoding: utf-8

RsCommon.setup do |config|
  # Default page title
  config.default_title = 'Default title'

  # Constraint for RsCommon builtin routes. Default always match the request.
  config.routes_constraint = RsCommon::EmptyConstraint
end