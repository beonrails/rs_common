begin
  require 'airbrake'

  Airbrake.configure do |config|
    config.api_key     = '<set_your_api_key_here>'
    config.host        = 'errbit.rocketscience.it'
    config.port        = 80
    config.secure      = config.port == 443
  end
rescue LoadError
  # do nothing
end

