# encoding: utf-8
require 'rails/generators'
require 'bundler'

module RsCommon
  module Generators
    class InstallGenerator < ::Rails::Generators::Base #:nodoc:
      source_root File.expand_path("../templates", __FILE__)

      desc "Install all standard RocketScience requirements"
      def install
        copy_gemfile
        setup_routes
        copy_assets
        create_vendor_assets
        copy_locales
        copy_database_config
        configure_active_record
        setup_rspec
        setup_controller_generator
        setup_time_zone
        setup_default_locale
        setup_devise
        setup_simple_form
        setup_client_side_validations
        setup_rs_common_initializer
        setup_errbit_initializer
        setup_ar_initializer
        setup_hirb_initializer
        setup_unicorn
        setup_models
        setup_devise_routes
        setup_layouts
        setup_capistrano
        setup_staging_environment
        setup_test_environment
        setup_rocketscience_controller
        setup_precompiled_assets
        create_migrations
        create_seeds
        create_home_controller
        remove_unnecessary_files
        create_shared_views_directory
        create_concerns_directories
      end

    protected

      def app_name
        ::Rails.application.class.parent_name.to_s
      end

      def copy_gemfile #:nodoc:
        remove_file 'Gemfile'
        remove_file 'Gemfile.lock'
        copy_file 'Gemfile', 'Gemfile'
        bundle_install
      end

      def setup_routes #:nodoc:
        remove_file "config/routes.rb"
        template "config/routes.rb", "config/routes.rb"
      end

      def copy_assets #:nodoc:
        remove_dir 'app/assets'
        directory 'app/assets', 'app/assets'
      end

      def setup_models #:nodoc:
        directory 'app/models', 'app/models'
      end

      def setup_devise_routes
        inject_into_file "config/routes.rb", :after => "constraints(RsCommon.routes_constraint) do\n" do
          <<-CONTENT.gsub(/^ {8}/, '')
            devise_for :users,
              :skip        => [ :sessions ],
              :controllers => { :sessions => 'users/sessions' }

            as :user do
              get   'login'  => 'devise/sessions#new',     :as => :new_user_session
              post  'login'  => 'devise/sessions#create',  :as => :user_session
              match 'logout' => 'devise/sessions#destroy', :as => :destroy_user_session,
                :via => Devise.mappings[:user].sign_out_via
            end
          CONTENT
       end
      end

      def create_vendor_assets #:nodoc:
        remove_dir 'vendor/assets'
        directory 'vendor/assets', 'vendor/assets'

        inject_into_file "config/application.rb", :after => "class Application < Rails::Application\n" do
          <<-CONTENT.gsub(/^ {8}/, '')

            config.sass.load_paths << File.join(Rails.root, 'vendor', 'assets', 'stylesheets')
            config.sass.load_paths << File.join(Rails.root, 'vendor', 'assets', 'javascripts')
            config.sass.load_paths << File.join(Rails.root, 'vendor', 'assets', 'images')
          
          CONTENT
        end
      end

      def copy_locales #:nodoc:
        remove_dir 'config/locales'
        directory 'config/locales', 'config/locales'
      end

      def copy_database_config #:nodoc:
        remove_file "config/database.yml"
        template "config/database.yml", "config/database.yml"
      end

      def configure_active_record #:nodoc:
        gsub_file 'config/application.rb', "config.active_record.whitelist_attributes = true" do
          <<-CONTENT.gsub(/^ {8}/, '')

            config.active_record.whitelist_attributes = true
            config.active_record.mass_assignment_sanitizer = :strict
            config.active_record.identity_map = true
          CONTENT
        end
      end

      def setup_rspec #:nodoc:
        generate "rspec:install"
        remove_dir "test"
        inject_into_file "config/application.rb", :after => "class Application < Rails::Application\n" do
          <<-CONTENT.gsub(/^ {8}/, '')

            config.generators do |g|
              g.test_framework :rspec
            end
          
          CONTENT
        end
      end

      def setup_controller_generator #:nodoc:
        gsub_file 'config/application.rb', "g.test_framework :rspec" do
          <<-CONTENT.gsub(/^ {8}/, '')
              g.test_framework :rspec
              g.scaffold_controller :rocketscience_controller
          CONTENT
        end
      end

      def setup_time_zone #:nodoc:
        gsub_file 'config/application.rb', "# config.time_zone = 'Central Time (US & Canada)'" do
          "config.time_zone = 'Ekaterinburg'"
        end
      end
      
      def setup_default_locale #:nodoc:
        gsub_file 'config/application.rb', '# config.i18n.default_locale = :de' do
          "config.i18n.default_locale = 'ru'"
        end

        gsub_file 'config/application.rb', "# config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]" do
          "config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]"
        end
      end

      def setup_capistrano #:nodoc:
        capify!
        remove_file "config/deploy.rb"
        template "config/deploy.rb", "config/deploy.rb"
      end

      def setup_devise #:nodoc:
        generate :devise, 'User'
        remove_file 'app/models/user.rb'
        copy_file 'config/initializers/devise.rb', 'config/initializers/devise.rb'

        gsub_file 'config/routes.rb', "devise_for :users\n", ''

        # Create migration that adds name field to users table
        filename = "db/migrate/#{(Time.now).strftime("%Y%m%d%H%M%S")}_add_name_to_users.rb"

        create_file filename, <<-CONTENT.gsub(/^ {10}/, '')
          class AddNameToUsers < ActiveRecord::Migration
            def change
              add_column :users, :name, :string, :default => nil, :null => true
            end
          end
        CONTENT
      end

      def setup_simple_form #:nodoc:
        generate 'simple_form:install', '--bootstrap'
        gsub_file 'config/initializers/simple_form.rb', "'btn'", "'btn btn-primary btn-large'"

        append_file 'config/initializers/simple_form.rb' do
          <<-CONTENT.gsub(/^ {12}/, '')
            SimpleForm::FormBuilder.map_type :email, :to => ::EmailInput
            SimpleForm::FormBuilder.map_type :url, :site, :website, :to => ::UrlInput
            SimpleForm::FormBuilder.map_type :tel, :phone, :to => ::TelInput    
          CONTENT
        end
      end

      def setup_client_side_validations #:nodoc:
        generate 'client_side_validations:install'
        gsub_file 'lib/templates/erb/scaffold/_form.html.erb', ") do |f|", ", :validate => true) do |f|"
      end

      def setup_rs_common_initializer #:nodoc:
        copy_file 'config/initializers/rs_common.rb', 'config/initializers/rs_common.rb'
      end

      def setup_errbit_initializer #:nodoc:
        copy_file 'config/initializers/errbit.rb', 'config/initializers/errbit.rb'
      end

      def setup_ar_initializer
        copy_file 'config/initializers/active_record.rb', 'config/initializers/active_record.rb'
      end

      def setup_hirb_initializer
        copy_file 'config/initializers/hirb.rb', 'config/initializers/hirb.rb'
      end

      def setup_unicorn #:nodoc:
        template "config/unicorn.rb", "config/unicorn.rb"
      end

      def setup_layouts #:nodoc:
        remove_dir 'app/views/layouts'
        directory 'app/views/layouts', 'app/views/layouts'
      end

      def setup_staging_environment #:nodoc:
        production_conf = "#{destination_root}/config/environments/production.rb"
        staging_conf    = "#{destination_root}/config/environments/staging.rb"

        copy_file production_conf, staging_conf

        gsub_file staging_conf, '# config.log_level = :debug' do
          "config.log_level = :debug"
        end
      end

      def setup_test_environment #:nodoc:
        inject_into_file "#{destination_root}/config/environments/test.rb",
          "\n  config.action_mailer.default_url_options = { :host => 'www.example.com' }\n",
          :before => "end\n",
          :verbose => false
      end

      def setup_rocketscience_controller #:nodoc:
        gsub_file 'app/controllers/application_controller.rb', 'class ApplicationController < ActionController::Base' do
          "# encoding: utf-8\n\nclass ApplicationController < RocketscienceController"
        end

        inject_into_file "app/controllers/application_controller.rb", :after => "protect_from_forgery\n" do
          <<-CONTENT.gsub(/^ {10}/, '')

            before_filter do
              unless view_context.current_page?(main_app.root_path)
                add_crumb 'Главная', view_context.main_app.root_path
              end
            end
          
          CONTENT
        end
      end

      def setup_precompiled_assets #:nodoc:
        replace = "# config.assets.precompile += %w( search.js )"
        replacement = "config.assets.precompile += %w( jquery.placeholder.js bootstrap-datepicker.css bootstrap-datepicker.js )"

        gsub_file 'config/environments/production.rb', replace, replacement
        gsub_file 'config/environments/staging.rb', replace, replacement
      end

      def create_migrations #:nodoc:
        # Create roles migrations
        filename = "db/migrate/#{(Time.now).strftime("%Y%m%d%H%M%S")}_create_role_and_user_role.rb"
        create_file filename, <<-CONTENT.gsub(/^ {10}/, '')
          class CreateRoleAndUserRole < ActiveRecord::Migration
            def change
              create_table :roles do |t|
                t.string :name,        :null => false
                t.string :description, :null => false
              end

              add_index :roles, :name, :unique => true

              create_table :user_roles do |t|
                t.integer :role_id, :null => false
                t.integer :user_id, :null => false
              end

              add_index :user_roles, [:role_id, :user_id], :unique => true
            end
          end
        CONTENT
      end

      def create_seeds #:nodoc:
        remove_file 'db/seeds.rb'
        copy_file 'db/seeds.rb', 'db/seeds.rb'
      end

      def create_home_controller #:nodoc:
        # Generate home_controller
        generate :controller, "home index"

        # Change generated routes
        gsub_file 'config/routes.rb', 'get "home/index"' do
          ''
        end
      end

      def remove_unnecessary_files #:nodoc:
        remove_file "public/index.html"
      end

      def create_shared_views_directory #:nodoc:
        empty_directory "app/views/shared"
      end

      def create_concerns_directories #:nodoc:
        empty_directory "app/controllers/concerns"
        empty_directory "app/models/concerns"
      end

      def bundle_install #:nodoc:
        Bundler.with_clean_env do
          run "bundle install"
        end
      end
    end
  end
end