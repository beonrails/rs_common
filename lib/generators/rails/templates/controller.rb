# encoding: utf-8

<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController
  # You should add, for example, :class => "YourEngine::ModelClass" if your model has namespace
  # You should add :find_by => :slug if your record searched by slug column, not an id
  load_and_authorize_resource :<%= class_name.underscore %>
<% unless options[:singleton] -%>
  skip_authorize_resource     :<%= class_name.underscore %>, :only => [:index, :show]
<% else -%>
  skip_authorize_resource     :<%= class_name.underscore %>, :only => :show
<% end -%>

  respond_to :html

  # RocketScience toolbar
  rs_toolbar
 
<% unless options[:singleton] -%>
  def index
    @<%= table_name %> = <%= orm_class.all(class_name).sub('all', 'scoped') %>
    respond_with(@<%= table_name %>)
  end
<% end -%>

  def show
    respond_with(@<%= file_name %>)
  end

  def new
    respond_with(@<%= file_name %>)
  end

  def edit
    respond_with(@<%= file_name %>)
  end

  def create
    @<%= file_name %>.save
    respond_with(@<%= file_name %>)
  end

  def update
    @<%= file_name %>.update_attributes(params[:<%= file_name %>])
    respond_with(@<%= file_name %>)
  end

  def destroy
    @<%= orm_instance.destroy %>
    respond_with(@<%= file_name %>)
  end
end
<% end -%>