require 'rs_common/engine'
require 'rs_common/simple_form_extension'
require 'rs_common/railtie'
require 'rs_common/empty_constraint'
require 'rails/all'

module RsCommon
  mattr_accessor :default_title
  @@default_title = :default_title

  mattr_accessor :routes_constraint
  @@routes_constraint = RsCommon::EmptyConstraint

  def self.setup
    yield self
  end
end
