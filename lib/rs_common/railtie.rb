require 'rs_common'
require 'rails'

module RsCommon
  module Rails
    class Railtie < ::Rails::Railtie
      config.app_generators do |g|
        path = File::expand_path('../../generators/templates', __FILE__)
        g.templates.unshift path
      end

      # AR extension
      initializer 'rs_common.active_record' do
        ActiveSupport.on_load(:active_record) do
          #ActiveRecord::Base.send :include, RsCommon::ActiveRecordExtension
        end
      end

      # Make spec rake tasks available in app
      rake_tasks do
        import File::expand_path('../../../spec/lib/tasks/rs_common_spec_tasks.rake', __FILE__)
      end
    end
  end
end