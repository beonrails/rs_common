require 'simple_form'

SimpleForm::FormBuilder.class_eval do # :nodoc:
  # Creates a button with back link:
  #
  # form_for @user do |f|
  #   f.button_or_back :submit, :url => root_path
  # end
  #
  def button_or_back(type, *args, &block)
    options = args.extract_options!.dup
    url     = options[:url]

    options.delete :url
    args << options

    button(type, *args, &block) +
    template.content_tag(:span, template.t('simple_form.button_or_back.or')) +
    template.content_tag(:a, template.t('simple_form.button_or_back.link'), :href => url)
  end
end