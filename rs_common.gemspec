$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rs_common/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rs_common"
  s.version     = RsCommon::VERSION
  s.authors     = ["Andrew Kozlov"]
  s.email       = ["demerest@gmail.com"]
  s.homepage    = "http://gitlab.rocketscience.it/rs_common"
  s.description = "Common functionality for all RocketScience projects"
  s.summary     = s.description

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 3.2.6"
  s.add_development_dependency "gem-release"
end
