//= require jquery
//= require rs_common/lazy_load
//= require rs_common/flash_messages
//= require jquery_ujs
//= require jquery_nested_form

/////////////////////////////////////////////////////////////////////////////////////////////
// Lazy loading for common js
$(function() {
  // Handle inputs with placeholder attribute
  // This adds support HTML5 placeholders for old browsers
  if ($('textarea[placeholder], input[placeholder]').length) {
    LazyLoad.js('/assets/jquery.placeholder.js', function() {
      $('input, textarea').placeholder();
    })
  }

  // Handle form date inputs. This adds calendar to all date inputs.
  if ($('input.datepicker').length) {
    LazyLoad.css('/assets/bootstrap-datepicker.css')
    LazyLoad.js('/assets/bootstrap-datepicker.js', function() {
      $('input.datepicker').datepicker({
        format: 'mm.dd.yyyy'
      })
    })
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////
// Support pretty file inputs in forms
$(function() {
  $('form div.file button.btn').live('click', function(e) {
    e.preventDefault()

    var button    = $(this)
    var wrapper   = button.closest('div.file')
    var fileinput = wrapper.find('input[type="file"]')
    var textinput = wrapper.find('input[type="text"]')

    fileinput.trigger('click')

    fileinput.change(function(e) {
      textinput.val(
        $(this).val().split(/[\\/]/)[0]
      )
    })
  })
})

/////////////////////////////////////////////////////////////////////////////////////////////
// For client_side_validations: smooth scrolling to first field with error on
// form validation.
/*$(function() {
  var offset   = 100
  var duration = 500
  var easing   = 'swing'

  $('form[data-validate=true]').on('form:validate:fail.ClientSideValidations', function(e) {
    e.preventDefault();
    var firstElement = $(this).find('div.error').first().find('[data-validate=true]')

    $("html, body").animate({
      scrollTop: firstElement.offset().top - offset + "px"
    }, {
      'duration' : duration,
      'easing'   : easing
    });

    firstElement.focus()
  });
})*/

/////////////////////////////////////////////////////////////////////////////////////////////
// To support client_side_validations in nested forms
/*ClientSideValidations.formBuilders['NestedForm::SimpleBuilder'] = ClientSideValidations.formBuilders['SimpleForm::FormBuilder'];

$('form[data-validate=true]').on('nested:fieldAdded', function(e) {
  $(e.target).find(':input').enableClientSideValidations();
});*/
