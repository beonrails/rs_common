module Extensions
  module User
    module Roles
      def self.included(base)
        base.send(:include, InstanceMethods)
        #base.extend(ClassMethods)

        base.class_eval do
          has_many :user_roles
          has_many :roles, :through => :user_roles

          # Set default invited user role as client
          #before_create do
          #  unless roles.any?
          #    self.roles << Role.find_by_name(:client)
          #  end
          #end
        end
      end

      module ClassMethods
        # ...
      end

      module InstanceMethods
        # Check user role
        def role?(role)
          !!self.roles.select(1).find_by_name(role.to_s.downcase)
        end

        def admin?
          role?(:admin)
        end
      end
    end
  end
end