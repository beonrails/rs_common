module Extensions
  module User
    module Common
      def self.included(base)
        base.send(:include, InstanceMethods)
        #base.extend(ClassMethods)

        base.class_eval do
          # Setup accessible (or protected) attributes for your model
          attr_accessible :email, :login, :password, :password_confirmation,
            :remember_me, :name, :role_ids

          # see https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address
          attr_accessor   :identity
          attr_accessible :identity
          cattr_accessor  :current
        end
      end

      module ClassMethods
        # ...
      end

      module InstanceMethods
        # Return user name or user name from email address
        def username
          name.blank? ? email.match(/^[^@]+/)[0] : name
        end

        alias :to_s :username
      end
    end
  end
end


          
