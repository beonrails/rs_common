module Extensions
  module User
    module Devise
      def self.included(base)
        base.class_eval do
          devise :database_authenticatable, :validatable,
            :recoverable, :rememberable, :trackable
        end
      end
    end
  end
end