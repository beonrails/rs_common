class PasswordInput < SimpleForm::Inputs::PasswordInput 
  def input
    template.content_tag :div, :class => 'input-append' do
      template.concat(super)

      span = template.content_tag :span, :class => 'add-on' do
        template.content_tag :i, '', :class => 'icon icon-key'
      end

      template.concat(span)
    end
  end
end
