class DateRangeInput < SimpleForm::Inputs::StringInput 
  def input                    
    value = object.send(attribute_name) if object.respond_to? attribute_name
    input_html_options[:value] ||= I18n.localize(value, :format => :short) if value.present?
    input_html_options[:type] = 'text'
    input_html_classes << "datepicker"

    template.content_tag :div, :class => 'input-append' do
      template.concat(super)

      span = template.content_tag :span, :class => 'add-on' do
        template.content_tag :i, '', :class => 'icon icon-calendar'
      end

      template.concat(span)
    end
  end
end
