class FileInput < SimpleForm::Inputs::FileInput
  def input
    template.content_tag :div, :class => 'input-append' do
      template.concat(super)

      div = template.content_tag :div, :class => 'dummyfile' do
        template.concat(template.tag :input, :class => 'input disabled', :type => :text, :name => :filename, :readonly => true)

        button = template.content_tag :button, :class => 'btn' do
          template.concat(template.content_tag :i, '', :class => 'icon icon-paper-clip')
          template.concat(template.raw("&nbsp;") + I18n.t('simple_form.browse_button'))
        end

        template.concat(button)
      end

      template.concat(div)
    end
  end
end