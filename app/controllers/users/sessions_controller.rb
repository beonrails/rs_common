# encoding: utf-8

class Users::SessionsController < Devise::SessionsController
  include Devise::Controllers::Rememberable

  # Автоматическое запоминание залогинившегося
  # пользователя (как будто установлен чекбокс "запомнить меня")
  def after_sign_in_path_for(resource)
    remember_me(resource)
    sign_in_url = url_for(
      :action     => 'new',
      :controller => 'sessions',
      :only_path  => false,
      :protocol   => 'http'
    )

    if request.referer == sign_in_url
      super
    else
      request.referer || stored_location_for(resource) || root_path
    end
  end
end