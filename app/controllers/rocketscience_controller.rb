# encoding: utf-8

class RocketscienceController < ActionController::Base
  protect_from_forgery
  rescue_from Exception, :with => :handle_exceptions
  before_filter :set_current_user

  helper :rocketscience
  helper :devise

  responders :flash

protected

  def set_current_user
    User.current = current_user
  end

  def not_found!
    raise ActiveRecord::RecordNotFound.new('Not Found')
  end

  def handle_exceptions(e)
    case e
      when CanCan::AccessDenied
        if user_signed_in?
          not_found
        else
          authenticate_user!
        end
      when ActiveRecord::RecordNotFound
        not_found
      else
        internal_error(e)
      end
  end

  def not_found
    # Just render view
    render 'rocketscience/not_found', :status => 404
  end

  def internal_error(exception)
    if Rails.env.production?
      notify_airbrake(exception) if defined?(Airbrake)

      # And just render view
      render :layout   => 'layouts/application',
             :template => 'rocketscience/internal_error',
             :status   => 500
    else
      raise
    end
  end
end

